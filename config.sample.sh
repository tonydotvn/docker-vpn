#!/usr/bin/env bash

export VPN_SERVER="$(dig +short myip.opendns.com @resolver1.opendns.com)"
export PASSWORD="123457"

if [[ -z "${VPN_ROOT}" ]]; then
  export VPN_ROOT="$(pwd)"
fi

export VPN_DATA="${VPN_ROOT}/data"

[[ -d "$VPN_DATA" ]] || mkdir -p "${VPN_DATA}"
